\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "something nice"
  subtitle = "For more information on using LilyPond, please see
http://lilypond.org/introduction.html"
%or http://lilypond.org/doc/v2.18/Documentation/learning/simple-notation" 	
}

\relative c, {
  \clef "bass"
  \time 3/4
  \tempo "Andante" 4 = 120
  c2 e8 c'
  g'2.
  f4 e d
  c4 c, r
}
