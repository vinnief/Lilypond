%{ commentaire 
%}

\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "pomme de reinette et pomme d'api"
  subtitle = "chan�on d'enfants"
}
\score{
\relative c' {\key c \major 
  \time 4/4 
  \tempo  "Allegro" 4 = 120
 \time 4/4  f8 f16 f f8 a c a f4 c8 c' c, c' a4 f f8 f16 f f8 a c a f4 c8 c' c, c' f,2}
 \addlyrics{pomme de rein ette et pomme d'a pi, ta pis ta pis rou ge, pomme de rein ette et 
pomme d'a pi, ta pis ta pis vert}
 \layout { }
  \midi { }
}