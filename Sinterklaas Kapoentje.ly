%{
Welcome to LilyPond
===================
sinter klaas kapoentje
%}

\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Sinterklaas Kapoentje"
  subtitle = "een stout liedje"
}
\score{
\relative c' {\key c \major 
  \time 4/4 
  \tempo  "Presto" 4 = 180
  g' g a a g2 e g4 g4 a a g2 e f4 f f d f2 f2 a4 g f e d2 c }
  \addlyrics{ Sin ter klaas Ka poen tje, gooi wat in mijn schoen tje, gooi wat in mijn laars je, dank je Sin ter klaas je}
  \layout{}
  \midi{}

}
